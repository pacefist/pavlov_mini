# PAVLOV MINI: Quadruped robot
Project in progress. Not fisnihed yet. I will document all the code through my youtube channel

![Demo](images/demo.gif)




## Description
Pavlov mini is a DIY 3D printed robot dog that uses hobby servomotors. It is inspired by MIT Cheetah and uses similar algorithms adapted to work with hobby servomotors. The control is embebed in ROS (Robot Operating System) and works under ROS melodic with a raspberry pi 4 running Ubuntu 18.04LTS. The control software is written in C++, while some other packages in python are used to control/visualize the robot. 


## Requirements
To run the code you need a computer running ROS melodic, for example with Ubuntu 18.04 LTS (although the version 19.04LTS should work as well, but not Ubuntu 20.04LTS). The first step is to install ROS. Follow the instructions in the following link:
http://wiki.ros.org/melodic/Installation/Ubuntu

Next, create a workspace with the following tutorial:
http://wiki.ros.org/catkin/Tutorials/create_a_workspace

## Setup
Clone the Pavlov mini project in the /src/ folder of your workspace:
> git clone https://gitlab.com/anflores/pavlov_mini/

Now build the project:
> cd ..

> catkin_make

## Test the code
In principle you don't need to build your own 3D printed robot to run the code. You can test and simulate the robot in your computer.
First launch the test file in a terminal:
> roslaunch gait_control gait_control_test.launch

If everything works, 2 windows should open. The first one is rviz, that should show the robot after a few seconds. The second window is a black window that to control the robot. Right now the robot is in 'stop' mode. To make it walk, run in a different terminal the following command:

> rosservice call /pavlov_mini/set_gait "gait_filename: 'diagonal'" 

After this, you should see the robot dog moving the diagonal legs. You can set different gaits for the robot, which are defined in files within the gait_control package (pavlov_mini/gait_control/gaits/). Remember not to include the '.gait' extension file when calling the rosservice "/pavlov_mini/set_gait".

Now you can use the black window to control the robot using the keys "w", "s", "d", "a", "q", "e".



## Use the code in a physical robot
To use the code in a real robot, you need a Teensy 4.0 board with a IMU BNO 080 sensor. 
First, you need to copy the content of the folder /pavlov_mini/arduino/libraries/ into your Arduino libraries (usually when you install Arduino IDE in Ubuntu, your libraries are located in in ~/Arduino/libraries/).

Then you need install Teensyduino (an add-on to upload arduino scripts to Teensy boards). Follow this link:
https://www.pjrc.com/teensy/teensyduino.html

Finally, you need to upload the Arduino script located in the folder /pavlov_mini/arduino/pavlov_mini_arduino/ into your Teensy board. 

If everything is good, you should be able to run the following command in a terminal:

> roslaunch gait_control gait_control_with_visualization.launch

If you receive an error about 'rosserial not recognized', the install rosserial with the following command

> sudo apt install ros-melodic-rosserial

If you receive an error about 'could not open port /dev/ttyACM0', then you need to change the serial port where your Teensy is connected. You need to open the file 
/pavlov_mini/pavlov_mini_calibration/launch/connect_teensy.launch 
and edit the 4th line and in 'value', you need to specify the port where Teensy is connected. You can look up this port with the arduino IDE.







